require "rails_helper"

RSpec.describe Api::RestaurantsController, :type => :controller do
  describe "GET #index" do
    it "loads this week`s menu" do
      restaurant1 = Restaurant.create(name: "Central")
      menu1 = Menu.create(restaurant: restaurant1, week: Date.today)
      menu2 = Menu.create(restaurant: restaurant1, week: Date.today - 7.days)

      get :index
      expect(assigns(:restaurants)[0].menus).to eq([menu1])
    end
  end
end
