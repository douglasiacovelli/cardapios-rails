class Menu < ApplicationRecord
  belongs_to :restaurant

  scope :this_week, -> { where("week > ?", Date.today.beginning_of_week) }

end
