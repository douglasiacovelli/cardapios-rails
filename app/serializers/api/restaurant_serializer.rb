class Api::RestaurantSerializer < ActiveModel::Serializer
  attributes :id, :name, :menu

  def menu
    object.menus.this_week.first
  end
end
